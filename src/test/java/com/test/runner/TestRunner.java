package com.test.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features = "Features",
					 glue = {"com/step/defination" },
				   plugin = { "pretty", "html:HTML_Report/test-output.html" },
			   monochrome = true, 
			       dryRun = false,
			         tags = "@Regtest")
public class TestRunner {

}
