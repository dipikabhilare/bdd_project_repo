package com.pages.loginpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.step.defination.Setup;

public class LoginDetailsPage {

	public WebElement UserName = Setup.getInstance().getDriver().findElement(By.xpath("//input[@id='username']"));

	public WebElement Password = Setup.getInstance().getDriver().findElement(By.xpath("//input[@id='password']"));

	public WebElement LoginButton = Setup.getInstance().getDriver().findElement(By.xpath("//button[@type='submit']"));

}
