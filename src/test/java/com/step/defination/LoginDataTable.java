package com.step.defination;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.testng.Reporter;
import com.pages.loginpage.LoginDetailsPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginDataTable {

	LoginDetailsPage details = new LoginDetailsPage();

	@Given("^Title of webpage is Infostretch$")
	public void verifyLoginPageTitle() {
		String pagetitle = Setup.getInstance().getDriver().getTitle();
		Reporter.log(pagetitle);
		Assert.assertEquals("Infostretch NEST", pagetitle);
	}

	@When("^I enter valid data on the page$")
	public void enterLoginCredentials(DataTable table) {
		Setup.getInstance().getDriver().manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		List<List<String>> data = table.asLists();
		details.UserName.sendKeys(data.get(0).get(0));
		details.Password.sendKeys(data.get(0).get(1));
		details.LoginButton.click();

		Reporter.log(Setup.getInstance().getDriver().getCurrentUrl());

	}

	@Then("^The user login should be successful$")
	public void login_Successful() {
		if (Setup.getInstance().getDriver().getCurrentUrl().equalsIgnoreCase("https://nest.infostretch.com/#/login")) {
			
			Reporter.log("Test Case Passed");
		} else {
			Reporter.log("Test Case Failed");
		}
	}

}
