package com.step.defination;

import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.testng.Reporter;
import com.pages.loginpage.LoginDetailsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LoginScenarioOutline {

	LoginDetailsPage details = new LoginDetailsPage();
	
	@Given("Title of web page is Infostretch")
	public void verifyPageTitle() {
		String title = Setup.getInstance().getDriver().getTitle();
		Reporter.log(title);
		Assert.assertEquals("Infostretch NEST", title);
	}

	@When("I enter Username as {string} and Password as {string}")
	public void enterCredentials(String arg1, String arg2) {
		Setup.getInstance().getDriver().manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		details.UserName.sendKeys(arg1);
		details.Password.sendKeys(arg2);
		details.LoginButton.click();
	}

	@Then("login should be unsuccessful")
	public void loginunsuccessful() {
		if (Setup.getInstance().getDriver().getCurrentUrl().equalsIgnoreCase("https://nest.infostretch.com/#/myview")) {
			Reporter.log("Test Case Failed");
		} else {
			Reporter.log("Test Case Passed");
		}

	}

}
