package com.step.defination;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class BaseClassHooks {

	
@Before
	public void Initialization() {
		Setup.getInstance().getDriver();
		Setup.getInstance().getDriver().get("https://nest.infostretch.com/");
		Setup.getInstance().getDriver().navigate().refresh();
		
		
	}
@After
	public void tearDown() {
		
	Setup.getInstance().closeDriver();
	
	}

	
}
