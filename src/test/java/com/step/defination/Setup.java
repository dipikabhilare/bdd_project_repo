package com.step.defination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Setup {
	
	private static Setup s= new Setup();
public static  WebDriver driver;
	
	private Setup() {
		
	}

	
	public static Setup getInstance() {
		return s;
	}
	
	private WebDriver createDriver() {
		
		System.setProperty("webdriver.chrome.driver",
				"src/test/resources/Driver/chromedriver.exe");
		 driver = new ChromeDriver();
		  driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);  
	       driver.manage().window().maximize();
	       return driver;
	
	}
	
	public WebDriver getDriver() {
		return (driver==null)? createDriver() : driver;
	}


	public static void closeDriver() {
		driver.close();
	}
}
