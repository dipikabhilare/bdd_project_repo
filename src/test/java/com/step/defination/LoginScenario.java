package com.step.defination;

import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.testng.Reporter;
import com.pages.loginpage.LoginDetailsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginScenario {
	
	LoginDetailsPage details = new LoginDetailsPage();
	
	@Given("^Title of web page is Infostretch Nest$")
	public void verifyPageTitle() {
		String title = Setup.getInstance().getDriver().getTitle();
		Reporter.log(title);
		Assert.assertEquals("Infostretch NEST", title);
	}

	@When("^user enters username and password$")
	public void enterCredentials() {
		Setup.getInstance().getDriver().manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		details.UserName.sendKeys("dipika.bhilare");
		details.Password.sendKeys("Mrunal#1");
		
		
	}

	@When("^user clicks  on login button$")
	public void clickLoginButton() {
		details.LoginButton.click();
	}

	@Then("^user is on home page$")
	public void verifyHomePageTitle() {
		String title = Setup.getInstance().getDriver().getTitle();
		Reporter.log(title);
	}

	
}
